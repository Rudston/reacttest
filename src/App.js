import React, {Component} from 'react'
import Album from './components/Album'
import DeleteModal from './components/DeleteModal'
import AddImageModal from './components/AddImageModal'
import AddAlbumModal from './components/AddAlbumModal'

// import AddImageModal from './AddImageModal'

class App extends Component {
  state = {
    albums: [],
    selectedAlbum: { // dummy initially
      id: 999999,
      name: '',
      selected: true
    },
    images: [],
    deleteAlbumModal: {
      show: false
    },
    addImageModal: {
      show: false
    },
    addAlbumModal: {
      show: false
    },

  }

  requestRemoveAlbum = () => {
    this.showDeleteAlbumModal(true)
  }

  showDeleteAlbumModal = (trueFalse) => {
    this.setState({
      ...this.state,
      deleteAlbumModal: {
        show: trueFalse
      }
    })
  }

  removeAlbum = () => {
    const {albums, selectedAlbum} = this.state
    if (albums.length > 0) {
      let remainingAlbums = albums.filter((album) => {
        return album.id !== selectedAlbum.id
      })
      if (remainingAlbums.length > 0) {
        remainingAlbums[0].selected = true
        this.setState({
          albums: remainingAlbums,
          selectedAlbum: remainingAlbums[0],
          deleteAlbumModal: {
            show: false
          }
        })
      } else {
        this.setState({
          ...this.state,
          albums: [],
          selectedAlbum: null,
          deleteAlbumModal: {
            show: false
          }
        })
      }
    } else {
      alert('There are no further albums!')
    }
  }

  selectAlbum = (idSelected) => {
    const {albums} = this.state
    let albumsUpdated = albums.map((album) => {
      return album.id === idSelected ? {...album, selected: true} : {...album, selected: false}
    })

    let selectedAlbumArray = albumsUpdated.filter((album) => {
      return album.selected ? album : null
    })

    this.setState({
      ...this.state,
      albums: albumsUpdated,
      selectedAlbum: selectedAlbumArray[0]
    })
  }

  requestAddImage = () => {
    this.showAddImageModal(true)
  }

  showAddImageModal = (trueFalse) => {
    this.setState({
      ...this.state,
      addImageModal: {
        show: trueFalse
      }
    })
  }

  requestAddAlbum = () => {
    this.showAddAlbumModal(true)
  }

  showAddAlbumModal = (trueFalse) => {
    this.setState({
      ...this.state,
      addAlbumModal: {
        show: trueFalse
      }
    })
  }

  componentDidMount() {

    const albumsUrl = "https://jsonplaceholder.typicode.com/albums"
    //GET https://jsonplaceholder.typicode.com/albums
    // POST https://jsonplaceholder.typicode.com/albums
    // DELETE https://jsonplaceholder.typicode.com/albums/{albumId}
    //
    // GET https://jsonplaceholder.typicode.com/albums/{albumId}/photos
    // POST https://jsonplaceholder.typicode.com/albums/{albumId}/photos
    // DELETE https://jsonplaceholder.typicode.com/albums/{albumId}/photos/{photoId}
    const url = ""

    //
    fetch(albumsUrl)
      .then((result) => result.json())
      .then((result) => {
        // limit to one user / owner
        let tempAlbums = result.filter((album) => album.userId === 2)
        let tempAlbums2 = []
        tempAlbums.forEach((item, index) => {
          tempAlbums2.push({
            name: item.title.replace(/^(.{11}[^\s]*).*/, "$1"), // shorten titles
            id: item.id,
            selected: index === 0 ? true : false
          })
        })

        this.setState({
          ...this.state,
          albums: tempAlbums2,
          selectedAlbum: tempAlbums2[0]
        })

      })
      .then(() => {
          console.log(this.state.selectedAlbum)
          // fetch images for selected album
          let selectedAlbumId = this.state.selectedAlbum.id
        fetch(albumsUrl + '/' + selectedAlbumId + '/photos')
          .then((result) => result.json())
          .then((result) => {
            this.setState({
              ...this.state,
              images: result.slice(0, 12) // just max 12
            })
            console.log(this.state.images)
          })

        }
      )
  }


  render() {
    const {albums, selectedAlbum} = this.state
    const deleteAlbumModalShow = this.state.deleteAlbumModal.show
    const addImageModalShow = this.state.addImageModal.show
    const addAlbumModalShow = this.state.addAlbumModal.show
    const rows = albums.map((row, index) => {
      return (
        <tr key={row.id}>
          <td className={row.selected && 'font-extrabold'} onClick={() => this.selectAlbum(row.id)}>{row.name}</td>
        </tr>
      )
    })

    return (
      <>
        <div className="w-full block px-16 py-16 h-screen bg-black text-white">
          <div className="w-full bg-myGray sm:rounded-lg h-full pt-16 flex justify-start">
            <div className="inline-block w-1/5 pl-8 h-full">
              <table>
                <tbody>{rows}</tbody>
              </table>
            </div>
            <div className="inline-block w-4/5 h-full">
              {albums.length > 0 ? (
                <Album albumData={selectedAlbum} removeAlbum={this.requestRemoveAlbum} addImage={this.requestAddImage}
                       addAlbum={this.requestAddAlbum}/>
              ) : (
                'No Albums To Show'
              )}
            </div>
          </div>
        </div>

        <DeleteModal deleteType="Album" deleteId={selectedAlbum.id} deleteName={selectedAlbum.name}
                     show={deleteAlbumModalShow}
                     onHide={() => this.showDeleteAlbumModal(false)}
                     onConfirm={() => this.removeAlbum()}

        />

        <AddImageModal albumName={selectedAlbum.name}
                       show={addImageModalShow}
                       onHide={() => this.showAddImageModal(false)}

        />

        <AddAlbumModal show={addAlbumModalShow}
                       onHide={() => this.showAddAlbumModal(false)}

        />
      </>
    )
  }
}

export default App
