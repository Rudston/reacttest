import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Modal, Button} from 'react-bootstrap';
import { useState } from 'react'

const AddImageModal = (props) => {
  const {albumName, onHide} = props
  const [imageName, setImageName] = useState('')
  const [imageUrl, setImageUrl] = useState('')

  const onSubmit = (e) => {
    e.preventDefault()
    if (!imageName) {
      alert('Please enter a name for the image')
      return
    } else if (!imageUrl) {
      alert('Please enter a url for the image')
      return
    } else {
      alert('image to add')
    }
    setImageName('')
    setImageUrl('')
  }
  return (
    <Modal
      {...props}
      size="medium"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <h4 style={{marginBottom: "2rem"}}>Add image to photo album {albumName}</h4>
        <form onSubmit={onSubmit}>
          <div>
            <input
              type='text'
              placeholder='Enter Image Name'
              value={imageName}
              onChange={(e) => setImageName(e.target.value)}
              className="myInput"
            />
          </div>
          <div>
            <input
              type='text'
              placeholder='Enter Image Url'
              value={imageUrl}
              onChange={(e) => setImageUrl(e.target.value)}
              className="myInput"
            />
          </div>
          <Button onClick={onHide} variant="secondary light" style={{borderRadius: "1rem", float: "left", height: "3rem"}}>Cancel</Button>
          <Button type="submit" style={{borderRadius: "1rem", float: "right", backgroundColor: "#beff8f", width: "60%", color: "black", height: "3rem"}}>Save Image</Button>
        </form>
      </Modal.Body>
    </Modal>
  )
}

export default AddImageModal
