import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Modal, Button} from 'react-bootstrap';

const DeleteModal = (props) => {
  const {deleteType, deleteName, onConfirm, onHide} = props
  return (
    <Modal
      {...props}
      size="medium"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <h4 style={{marginBottom: "2rem"}}>Delete {deleteType}</h4>
        <p>
          Are you sure you want to delete {deleteName}? This action cannot be undone.
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onHide}  variant="secondary light">Cancel</Button>
        <Button onClick={onConfirm} variant="danger">Confirm delete</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default DeleteModal
