import React from 'react'
import DeleteButton from './DeleteButton'
import AddButton from './AddButton'
import Image from './Image'

const Album = (props) => {
  const {albumData, removeAlbum, addImage, addAlbum} = props
  return (
    <div className="w-full h-full pl-0 pr-16 pb-8">
      <div className="grid grid-cols-4 gap-6 h-full">
        <Image />
        <Image />
        <Image />
        <Image />
        <Image />
        <Image />
        <Image />
        <Image />
        <Image />
        <Image />
        <Image />
        <Image />

        <div className="col-span-4">
          <div className="grid grid-cols-3 gap-2 h-full">
            <div className="h-full col-span-1 flex justify-start">
              <DeleteButton btnText={'Delete Album ' + albumData.name} deleteFunction={removeAlbum} album={albumData}/>
            </div>
            <div className="h-full col-span-1 flex justify-end">
              <AddButton btnText={`Add Image to ${albumData.name}`} addFunction={addImage} />
            </div>
            <div className="h-full col-span-1 flex justify-end">
              <AddButton btnText={'New Album'} addFunction={addAlbum} />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Album
