import React from "react"
import 'bootstrap/dist/css/bootstrap.min.css'
import {Modal, Button} from 'react-bootstrap'
import { useState } from 'react'

const AddAlbumModal = (props) => {
  const {onHide} = props
  const [albumName, setAlbumName] = useState('')

  const onSubmit = (e) => {
    e.preventDefault()
    if (!albumName) {
      alert('Please enter a name for the album')
      return
    } else {
      alert("new album with name " + albumName)
    }
    setAlbumName('')
  }
  return (
    <Modal
      {...props}
      size="medium"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <h4 style={{marginBottom: "2rem"}}>New Album</h4>
        <form onSubmit={onSubmit}>
          <div>
            <input
              type='text'
              placeholder='Enter album name'
              value={albumName}
              onChange={(e) => setAlbumName(e.target.value)}
              className="myInput"
            />
          </div>
          <Button onClick={onHide} variant="secondary light" style={{borderRadius: "1rem", float: "left", height: "3rem"}}>Cancel</Button>
          <Button type="submit" style={{borderRadius: "1rem", float: "right", backgroundColor: "#beff8f", width: "60%", color: "black", height: "3rem"}}>Create Album</Button>
        </form>
      </Modal.Body>
    </Modal>
  )
}

export default AddAlbumModal
