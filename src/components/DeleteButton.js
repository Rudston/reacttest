import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'


const DeleteButton = (props) => {
  const {btnText, deleteFunction, album} = props
  return (
    <div className="h-full flex flex-wrap content-center">
      <div onClick={() => deleteFunction(album.id)} className="inline-block content-center align-middle" style={{backgroundColor: "#a93034", border: "#a93034", borderRadius: "50%", height: "60px", width: "60px"}}>
        <div className="h-full flex flex-wrap content-center justify-center">
          <FontAwesomeIcon icon={faTrash} className="fa fa-trash fa-2x text-myGray" />
        </div>
      </div>
      <div className="pl-2 inline-block align-middle pt-4" onClick={() => deleteFunction(album.id)}>{btnText}</div>
    </div>

  )
}

export default DeleteButton
