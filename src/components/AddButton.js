import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPlus} from '@fortawesome/free-solid-svg-icons'

const AddButton = (props) => {
  const {btnText, addFunction} = props
  return (
  <div className="h-full flex flex-wrap content-center justify-center">
    <div onClick={() => addFunction()} className="pr-2 inline-block align-middle pt-4">{btnText}</div>
    <div onClick={() => addFunction()} className="inline-block content-center align-middle" style={{backgroundColor: "#b5fc96", border: "#b5fc96", borderRadius: "50%", height: "60px", width: "60px"}}>
      <div className="h-full flex flex-wrap content-center justify-center">
        <FontAwesomeIcon icon={faPlus} className="fa fa-plus fa-2x text-myGray" />
      </div>
    </div>
  </div>

  )
}

export default AddButton
